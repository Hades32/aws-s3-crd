/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	appsinformers "k8s.io/client-go/informers/apps/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	appslisters "k8s.io/client-go/listers/apps/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"

	s3v1alpha1 "gitlab.com/msvechla/aws-s3-crd/pkg/apis/s3controller/v1alpha1"
	clientset "gitlab.com/msvechla/aws-s3-crd/pkg/client/clientset/versioned"
	s3scheme "gitlab.com/msvechla/aws-s3-crd/pkg/client/clientset/versioned/scheme"
	informers "gitlab.com/msvechla/aws-s3-crd/pkg/client/informers/externalversions/s3controller/v1alpha1"
	listers "gitlab.com/msvechla/aws-s3-crd/pkg/client/listers/s3controller/v1alpha1"
)

const controllerAgentName = "aws-s3-controller"

const (
	// SuccessSynced is used as part of the Event 'reason' when a S3 is synced
	SuccessSynced = "Synced"
	// ErrResourceExists is used as part of the Event 'reason' when a S3 fails
	// to sync due to a Deployment of the same name already existing.
	ErrResourceExists = "ErrResourceExists"

	// MessageResourceExists is the message used for Events when a resource
	// fails to sync due to a Deployment already existing
	MessageResourceExists = "Resource %q already exists and is not managed by S3"
	// MessageResourceSynced is the message used for an Event fired when a S3
	// is synced successfully
	MessageResourceSynced = "S3 synced successfully"
	// MessageResourceCreated is the message used for an Event fired when a S3 bucket
	// is created successfully
	MessageResourceCreated = "S3 bucket created successfully"
	// AWSRegion is the region we use to connect to the AWS API
	AWSRegion = "eu-central-1"
)

// Controller is the controller implementation for S3 resources
type Controller struct {
	// kubeclientset is a standard kubernetes clientset
	kubeclientset kubernetes.Interface
	// sampleclientset is a clientset for our own API group
	sampleclientset clientset.Interface

	deploymentsLister appslisters.DeploymentLister
	deploymentsSynced cache.InformerSynced
	s3Lister          listers.S3Lister
	s3sSynced         cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// NewController returns a new sample controller
func NewController(
	kubeclientset kubernetes.Interface,
	sampleclientset clientset.Interface,
	deploymentInformer appsinformers.DeploymentInformer,
	s3Informer informers.S3Informer) *Controller {

	// Create event broadcaster
	// Add mongodb-controller types to the default Kubernetes Scheme so Events can be
	// logged for mongodb-controller types.
	s3scheme.AddToScheme(scheme.Scheme)
	glog.V(4).Info("Creating event broadcaster")
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartLogging(glog.Infof)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: kubeclientset.CoreV1().Events("")})
	recorder := eventBroadcaster.NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	controller := &Controller{
		kubeclientset:     kubeclientset,
		sampleclientset:   sampleclientset,
		deploymentsLister: deploymentInformer.Lister(),
		deploymentsSynced: deploymentInformer.Informer().HasSynced,
		s3Lister:          s3Informer.Lister(),
		s3sSynced:         s3Informer.Informer().HasSynced,
		workqueue:         workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "S3s"),
		recorder:          recorder,
	}

	glog.Info("Setting up event handlers")
	// Set up an event handler for when S3 resources change
	s3Informer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: controller.enqueueS3,
		UpdateFunc: func(old, new interface{}) {
			controller.enqueueS3(new)
		},
		DeleteFunc: controller.deleteS3Bucket,
	})

	return controller
}

// Run will set up the event handlers for types we are interested in, as well
// as syncing informer caches and starting workers. It will block until stopCh
// is closed, at which point it will shutdown the workqueue and wait for
// workers to finish processing their current work items.
func (c *Controller) Run(threadiness int, stopCh <-chan struct{}) error {
	defer runtime.HandleCrash()
	defer c.workqueue.ShutDown()

	// Start the informer factories to begin populating the informer caches
	glog.Info("Starting S3 controller")

	// Wait for the caches to be synced before starting workers
	glog.Info("Waiting for informer caches to sync")
	if ok := cache.WaitForCacheSync(stopCh, c.deploymentsSynced, c.s3sSynced); !ok {
		return fmt.Errorf("failed to wait for caches to sync")
	}

	glog.Info("Starting workers")
	// Launch two workers to process S3 resources
	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	glog.Info("Started workers")
	<-stopCh
	glog.Info("Shutting down workers")

	return nil
}

// runWorker is a long-running function that will continually call the
// processNextWorkItem function in order to read and process a message on the
// workqueue.
func (c *Controller) runWorker() {
	for c.processNextWorkItem() {
	}
}

// processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			runtime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		// Run the syncHandler, passing it the namespace/name string of the
		// S3 resource to be synced.
		if err := c.syncHandler(key); err != nil {
			return fmt.Errorf("error syncing '%s': %s", key, err.Error())
		}
		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		glog.Infof("Successfully synced '%s'", key)
		return nil
	}(obj)

	if err != nil {
		runtime.HandleError(err)
		return true
	}

	return true
}

func (c *Controller) getCRDS3FromKey(key string) (*s3v1alpha1.S3, error) {
	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		runtime.HandleError(fmt.Errorf("invalid resource key: %s", key))
		return nil, err
	}

	// Get the S3 resource with this namespace/name
	crdS3, err := c.s3Lister.S3s(namespace).Get(name)
	if err != nil {
		// The S3 resource may no longer exist, in which case we stop
		// processing.
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("s3 '%s' in work queue no longer exists", key))
			return crdS3, err
		}

		return crdS3, err
	}
	return crdS3, nil
}

// syncHandler compares the actual state with the desired, and attempts to
// converge the two. It then updates the Status block of the S3 resource
// with the current status of the resource.
func (c *Controller) syncHandler(key string) error {
	crdS3, err := c.getCRDS3FromKey(key)
	if err != nil {
		return err
	}

	regionName := crdS3.Spec.Region
	if regionName == "" {
		runtime.HandleError(fmt.Errorf("%s: region must be specified", key))
		return nil
	}

	// Get the bucket, if it does not exist we will create it
	sess, err := session.NewSession(&aws.Config{Region: aws.String(regionName)})
	if err != nil {
		return err
	}

	svc := s3.New(sess)
	input := &s3.ListObjectsInput{
		Bucket:  aws.String(crdS3.ObjectMeta.Name),
		MaxKeys: aws.Int64(2),
	}

	result, err := svc.ListObjects(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case s3.ErrCodeNoSuchBucket:

				// The specified bucket was not found, lets create it
				createErr := newS3Bucket(crdS3)
				if createErr != nil {
					return createErr
				}
				c.recorder.Event(crdS3, corev1.EventTypeNormal, SuccessSynced, MessageResourceCreated)
				return nil
			default:
				fmt.Println(aerr.Error())
				return aerr
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
			return aerr
		}
	}

	if result.Name != nil {
		fmt.Printf("Bucket already created: %s\n", *result.Name)
	}

	// If an error occurs during Get/Create, we'll requeue the item so we can
	// attempt processing again later. This could have been caused by a
	// temporary network failure, or any other transient reason.
	if err != nil {
		return err
	}

	// If the Deployment is not controlled by this S3 resource, we should log
	// a warning to the event recorder and ret
	// if !metav1.IsControlledBy(deployment, s3) {
	// 	msg := fmt.Sprintf(MessageResourceExists, deployment.Name)
	// 	c.recorder.Event(s3, corev1.EventTypeWarning, ErrResourceExists, msg)
	// 	return fmt.Errorf(msg)
	// }

	// // If this number of the replicas on the S3 resource is specified, and the
	// // number does not equal the current desired replicas on the Deployment, we
	// // should update the Deployment resource.
	// if s3.Spec.Replicas != nil && *s3.Spec.Replicas != *deployment.Spec.Replicas {
	//glog.V(4).Infof("S3 %s totelObjects: %d", crdS3.ObjectMeta.Name, crdS3.Status.S3TotalObjects)
	// 	deployment, err = c.kubeclientset.AppsV1().Deployments(s3.Namespace).Update(newDeployment(s3))
	// }

	// If an error occurs during Update, we'll requeue the item so we can
	// attempt processing again later. THis could have been caused by a
	// temporary network failure, or any other transient reason.
	if err != nil {
		return err
	}

	// Finally, we update the status block of the S3 resource to reflect the
	// current state of the world
	err = c.updateS3Status(crdS3)
	if err != nil {
		return err
	}

	c.recorder.Event(crdS3, corev1.EventTypeNormal, SuccessSynced, MessageResourceSynced)
	return nil
}

func (c *Controller) updateS3Status(crdS3 *s3v1alpha1.S3) error {

	sess, err := session.NewSession(&aws.Config{Region: aws.String(AWSRegion)})
	if err != nil {
		return err
	}

	svc := s3.New(sess)
	input := &s3.ListObjectsInput{
		Bucket: aws.String(crdS3.ObjectMeta.Name),
	}

	result, err := svc.ListObjects(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case s3.ErrCodeNoSuchBucket:
				fmt.Println(s3.ErrCodeNoSuchBucket, aerr.Error())
				return aerr
			default:
				fmt.Println(aerr.Error())
				return aerr
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
			return err
		}
	}

	// NEVER modify objects from the store. It's a read-only, local cache.
	// You can use DeepCopy() to make a deep copy of original object and modify this copy
	// Or create a copy manually for better performance
	s3Copy := crdS3.DeepCopy()
	s3Copy.Status.S3TotalObjects = (int32)(len(result.Contents))

	// If the CustomResourceSubresources feature gate is not enabled,
	// we must use Update instead of UpdateStatus to update the Status block of the Mongodb resource.
	// UpdateStatus will not allow changes to the Spec of the resource,
	// which is ideal for ensuring nothing other than resource status has been updated.
	_, err = c.sampleclientset.S3controllerV1alpha1().S3s(crdS3.Namespace).Update(s3Copy)

	return err
}

// enqueueS3 takes a S3 resource and converts it into a namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than S3.
func (c *Controller) enqueueS3(obj interface{}) {
	var key string
	var err error
	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}
	c.workqueue.AddRateLimited(key)
}

// deleteS3Bucket attempts to remove the specified bucket when the s3 ressource got deleted
func (c *Controller) deleteS3Bucket(obj interface{}) {

	var key string
	var err error
	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	// Convert the namespace/name string into a distinct namespace and name
	_, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		runtime.HandleError(fmt.Errorf("invalid resource key: %s", key))
		return
	}

	fmt.Printf("Received DELETE EVENT for: %s!\n", name)

	sess, err := session.NewSession(&aws.Config{Region: aws.String(AWSRegion)})
	if err != nil {
		return
	}

	svc := s3.New(sess)
	input := &s3.DeleteBucketInput{
		Bucket: aws.String(name),
	}

	_, deleteErr := svc.DeleteBucket(input)
	if deleteErr != nil {
		if aerr, ok := deleteErr.(awserr.Error); ok {
			switch aerr.Code() {
			default:
				fmt.Println(aerr.Error())
				return
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(deleteErr.Error())
			return
		}
	}

	fmt.Printf("DELETED BUCKET: %s\n", name)
}

// newS3Bucket creates the new bucket as specified
func newS3Bucket(crdS3 *s3v1alpha1.S3) error {
	sess, err := session.NewSession(&aws.Config{Region: aws.String(AWSRegion)})
	if err != nil {
		return err
	}

	svc := s3.New(sess)

	input := &s3.CreateBucketInput{
		Bucket: aws.String(crdS3.ObjectMeta.Name),
		CreateBucketConfiguration: &s3.CreateBucketConfiguration{
			LocationConstraint: aws.String(crdS3.Spec.Region),
		},
	}

	result, err := svc.CreateBucket(input)
	if err != nil {
		return err
	}

	fmt.Printf("Successfully created Bucket: %s\n", result)
	return nil
}
